<?php
/**
 * Installation profile for an XC Drupal Toolkit demo site
 * @file
 */

function xc_installation_profile_details() {
  return array(
    'name' => 'eXtensible Catalog Drupal Toolkit demo site (Customized for XC project demonstration)',
    'description' => 'The eXtensible Catalog Drupal Toolkit is a next generation library discovery interface',
  );
}

function xc_installation_profile_modules() {
  return array(
    // enable optional core modules
    'color', 'comment', 'menu', 'taxonomy', 'dblog', 'help', 'search', 'upload',

    // 3rd party contributed modules
    'lightbox2', 'addthis', 'fieldset_helper', 'print', 'print_mail', 'admin_menu',
    'jquery_ui_dialog',

    // enable XC modules
    'xc', 'xc_util', 'xc_metadata', 'xc_sql', 'xc_schema', 
    'ncip', 'xc_ncip_provider', 
    'oaiharvester', 'xc_oaiharvester_bridge', //  
    'xc_external', 'syndetics', //
    'xc_account', 'xc_auth', 
    'xc_ils', 'xc_import_export', 
    'xc_oai_dc', 'xc_solr', 'xc_index', 'xc_search', 
    'ezproxy_url_rewrite', 'xc_browse', 'xc_statistics',
    'xc_dewey', 'xc_xissn',

    // not enable by default: xc_biblio_export, xc_import_export
  );
}

function xc_installation_profile_task_list() {}

function xc_installation_profile_tasks(&$task, $url) {

  // NOTE: the node types settings came from Drupal's default profile
  //
  // Insert default user-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st("A <em>page</em>, similar in form to a <em>story</em>, is a simple method for creating and displaying information that rarely changes, such as an \"About us\" section of a website. By default, a <em>page</em> entry does not allow visitor comments and is not featured on the site's initial home page."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
    array(
      'type' => 'story',
      'name' => st('Story'),
      'module' => 'node',
      'description' => st("A <em>story</em>, similar in form to a <em>page</em>, is ideal for creating and displaying content that informs or engages website visitors. Press releases, site announcements, and informal blog-like entries may all be created with a <em>story</em> entry. By default, a <em>story</em> entry is automatically featured on the site's initial home page, and provides the ability to post comments."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }
  // end of node types settings

  $form_id = 'oaiharvester_repository_add_form';

  $mst_instances = array(
    array(
      'name' => 'MST demo',
      'oai_provider_url' => 'http://128.151.244.170:8080/MetadataServicesToolkit/pub/marctoxctransformation/oaiRepository',
    )
  );
  foreach ($mst_instances as $mst) {
    $form_state['values'] = $mst;
    $form_state['clicked_button']['#value'] = st('Submit and validate');
    drupal_execute($form_id, $form_state);
    $oai_provider = _oaiharvester_provider_get_by_url($mst['oai_provider_url']);
    oaiharvester_repository_validate($oai_provider, FALSE);
    xc_log_info('profile', $mst['name'] . ' repository validated');
  }

  xc_log_info('profile', 'creating schedule');
  // TODO:
  // Create an OAI-PMH scheduler
  $schedule = new stdClass();
  $schedule->schedule_name = 'MST demo - Weekly';
  $schedule->provider_id   = $oai_provider->provider_id;
  $schedule->recurrence    = 'Weekly';
  $schedule->minute        = 0;
  $schedule->hour          = 0;
  $schedule->day_of_week   = 1;
  $schedule->start_date    = '2009-11-09 00:00:00';
  $schedule->end_date      = '2010-11-09 00:00:00';
  $schedule->notify_email_address = 'admin@example.com';
  $schedule->parsing_mode  = 'regex';
  $schedule->is_cacheable  = 0;
  $schedule->max_request   = 10;
  $schedule->created_by    = 'admin';
  $schedule->created_date  = date("Y-m-d H:i:s");
  $schedule->state         = '';
  drupal_write_record('oaiharvester_harvester_schedules', $schedule);
  drupal_set_message('Schedules added');

  xc_log_info('profile', 'creating bridge');
  $bridge = new stdClass();
  $bridge->schedule_id  = $schedule->harvest_schedule_id;
  $bridge->do_defrbrize = 4;
  $bridge->use_insert   = 0;
  drupal_write_record('xc_oaiharvester_bridge_schedule_info', $bridge);
  drupal_set_message('Bridge info added');

  xc_log_info('profile', 'creating step');
  $step = new stdClass();
  $step->schedule_id = $schedule->harvest_schedule_id;
  $step->format_id = 1;
  drupal_write_record('oaiharvester_harvest_schedule_steps', $step);
  drupal_set_message('Schedule steps added');

  xc_log_info('profile', 'creating source');
  // create a source
  $source = new stdClass();
  $source->name = $schedule->schedule_name;
  $source->type = 'oai_schedule';
  $source->description = 'OAI-PMH schedule';
  drupal_write_record('xc_source', $source);
  drupal_set_message('Source added');

  xc_log_info('profile', 'creating schedule source');
  // link the source_id to the schedule_id created by the installer, this is 
  // done in the xc_oaiharvester_bridge_schedule_source table
  $bridge_source = new stdClass();
  $bridge_source->schedule_id = $schedule->harvest_schedule_id;
  $bridge_source->source_id = $source->source_id;
  drupal_write_record('xc_oaiharvester_bridge_schedule_source', $bridge_source);
  drupal_set_message('Bridge-source bound added');

  xc_log_info('profile', 'creating schedule location');
  // link the source_id to the location_id, this is done in the xc_source_locations table
  $location_source = new stdClass();
  $location_source->source_id = $source->source_id;
  $location_source->location_id = 1;
  drupal_write_record('xc_source_locations', $location_source);
  drupal_set_message('Location-source bound added');

  // site info
  $site_info['site_name'] = variable_get('site_name', 'XC demo site');;
  $site_info['site_mail'] = variable_get('site_mail', 'admin@example.com');
  $site_info['site_slogan'] = variable_get('site_slogan', '');
  // anonymous?
  $site_info['site_mission'] = '';
  $site_info['site_footer'] = '';
  $site_info['site_frontpage'] = 'xc/search';
  $form_state = array('values' => $site_info);
  drupal_execute('system_site_information_settings', $form_state);

  // here comes the theme specific settings
  $themes = system_theme_data();
  $preferred_themes = array('xc_default', 'garland');
  foreach ($preferred_themes as $theme) {
    if (array_key_exists($theme, $themes)) {
      system_initialize_theme_blocks($theme);
      db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name = ('%s')", $theme);
      variable_set('theme_default', $theme);
      break;
    }
  }

  $theme_info = theme_get_settings('xc_default');
  $theme_info['toggle_logo'] = 1;
  $theme_info['toggle_name'] = 0;
  $theme_info['toggle_slogan'] = 0;
  $theme_info['toggle_mission'] = 0;
  $theme_info['toggle_node_user_picture'] = 0;
  $theme_info['toggle_comment_user_picture'] = 0;
  $theme_info['toggle_favicon'] = 1;
  $theme_info['toggle_primary_links'] = 0;
  $theme_info['toggle_secondary_links'] = 0;
  $theme_info['default_logo'] = 1;
  $theme_info['logo_path'] = '';
  $theme_info['logo_upload'] = '';
  $theme_info['default_favicon'] = 1;
  $theme_info['favicon_path'] = 'sites/all/themes/xc_theme/xc_default/favicon.png';
  $theme_info['favicon_upload'] = '';
  $theme_info['toggle_search'] = 0;
  // XC theme specific settings
  $theme_info['xc_title']['show'] = 'never';
  $theme_info['xc_breadcrumb']['show'] = 'admin';
  $theme_info['xc_breadcrumb']['separator'] = '';
  $theme_info['xc_breadcrumb']['show_home'] = 1;
  $theme_info['xc_breadcrumb']['show_title'] = 1;
  $theme_info['xc_header_links']['home'] = 1;
  $theme_info['xc_header_links']['login_logout'] = 1;
  $theme_info['xc_header_links']['my_account'] = 1;
  $theme_info['xc_search']['hide_title'] = 1;
  $theme_info['xc_search']['hide_full_record_title'] = 1;
  $theme_info['xc_browse']['show_pages'] = 1;

  $form_state = array('values' => $theme_info);
  drupal_execute('system_theme_settings', $form_state);
  variable_set('theme_xc_default_settings', $theme_info);

  // blocks
  $table = 'blocks';
  $csv_file = drupal_get_path('profile', 'xc_installation') . '/' . $table . '.csv';
  $blocks = xc_util_csv2objects($csv_file);
  foreach ($blocks as $block) {
    $bid = db_result(db_query("SELECT bid from {blocks} WHERE module = '%s' AND delta = '%s' AND theme = '%s'",
      $block->module, $block->delta, $block->theme));
    if (is_null($bid) || $bid == 0) {
      $ret_val = drupal_write_record($table, $block);
    }
    else {
      $block->bid = $bid;
      $ret_val = drupal_write_record($table, $block, 'bid');
    }
    if (!$ret_val) {
      drupal_set_message(t('Unexpected error. Failed to insert a new record in %table. (%error)',
        array(
          '%table' => $table, 
          '%error' => db_error())
        )
      );
    }
  }

  // add roles
  $roles = array();
  $role_names = array('demo user', 'search admin', 'site admin');
  foreach ($role_names as $role_name) {
    $role = (object) array('name' => $role_name);
    drupal_write_record('role', $role);
    $roles[$role->rid] = $role;
  }

  // set access metadata permission for all users
  $result = db_query('SELECT * FROM {permission}');
  while ($permission = db_fetch_object($result)) {
    $changed = FALSE;
    $permissions = explode(', ', $permission->perm);
    if (!in_array('access metadata', $permissions)) {
      $permissions[] = 'access metadata';
      $changed = TRUE;
    }

    if (!in_array('access content', $permissions)) {
      $permissions[] = 'access content';
      $changed = TRUE;
    }

    if ($changed) {
      $permission->perm = join(', ', $permissions);
      drupal_write_record('permission', $permission, 'pid');
    }
  }

  foreach ($roles as $rid => $role) {
    $permissions = array('access metadata', 'access content');

    if ($role->name == 'demo user') {
      $permissions = array_unique(array_merge($permissions, explode(', ', 'access comments, post comments, post comments without approval, access devel information, display source code, access content, access metadata, access metadata schema')));
      $changed = TRUE;
    }
    elseif ($role->name == 'search admin') {
      $permissions[] = 'access administration menu';
      $permissions[] = 'administer xc search';
      $changed = TRUE;
    }
    elseif ($role->name == 'site admin') {
      $permissions = array_unique(array_merge($permissions, explode(', ', 'administer addthis, access administration menu, display drupal links, administer blocks, use PHP for block visibility, access comments, administer comments, post comments, post comments without approval, administer ezproxy, administer ezproxy url rewrite, administer fieldset state, save fieldset state, administer filters, administer lightbox2, download original image, administer menu, administer ncip, access content, administer content types, administer nodes, delete revisions, revert revisions, view revisions, administer harvester, harvest records, access print, administer print, node-specific print configuration, use PHP for link visibility, access send to friend, administer search, search content, use advanced search, access administration pages, access site reports, administer actions, administer files, administer site configuration, select different theme, administer taxonomy, upload files, view uploaded files, access user profiles, administer permissions, administer users, change own username, administer xc, administer xc accounts, edit all xc accounts, edit own xc account, administer xc authentication, edit all xc authenticated users, edit own xc authenticated users, administer xc_browse, administer ils, administer metadata export, administer metadata import, administer xc index, access metadata schema, administer metadata, administer metadata formats, administer metadata locations, administer ncip providers, test ncip providers, administer xc search, administer xc solr, administer xc wordnet')));
      $changed = TRUE;
    }

    if ($changed) {
      $permission = (object) array(
        'rid' => $rid,
        'perm' => join(', ', $permissions),
      );
      drupal_write_record('permission', $permission);
    }
  }

  drupal_set_message(st('Now you can start harvesting. Go to !this_page and click on Harvest',
    array('!this_page' => l(st('this page'), 'admin/xc/harvester/schedule/1'))));

  xc_log_info('profile', 'finished');

  menu_rebuild();
  return 'ok';
}

function xc_installation_form_alter(&$form, $form_state, $form_id) {
  switch ($form_id) {
    case 'install_configure':
      $form['site_information']['site_name']['#default_value'] = 'eXtensible Catalog Library';
      $form['site_information']['site_mail']['#default_value'] = 'admin@example.com';
      $form['admin_account']['account']['name']['#default_value'] = 'admin';
      $form['admin_account']['account']['mail']['#default_value'] = 'admin@example.com';
      $form['admin_account']['account']['pass']['#default_value'] = 'xc';
      $form['admin_account']['account']['pass']['#type'] = 'textfield';
      $form['admin_account']['account']['pass']['#title'] = t('Password');
      $form['admin_account']['account']['pass']['#description'] =  t('This password field has been changed to plain text by XC ease the installation process.');
      break;
  } 
} 
