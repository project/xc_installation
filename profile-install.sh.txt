DRUPAL_NAME=xc-1.1-test
DISTRO_VERSION=6.x-1.1-rc3
SOLR_VERSION=3.3.0
JQUERY_UI_VERSION=1.6

download() {
  if [ "$HAS_WGET" = "1" ]; then
    wget -q $1
  elif [ "$HAS_CURL" = "1" ]; then
    curl -O $1
  fi
}

download_requirement_check() {
  HAS_WGET=`command -v wget >/dev/null && echo "1" || echo "0"`
  HAS_CURL=`command -v curl >/dev/null && echo "1" || echo "0"`

  if [ "$HAS_WGET" = "0" -a "$HAS_CURL" = "0" ]; then
    echo "Please install either wget or curl"
    echo ""
    echo "Or download these files manually:"
    echo "http://ftp.drupal.org/files/projects/xc_installation-$DISTRO_VERSION-core.tar.gz"
    echo "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
    echo "http://www.apache.org/dist/lucene/solr/$SOLR_VERSION/apache-solr-$SOLR_VERSION.tgz"
    exit;
  fi
}

requirement_check() {
  HAS_PATCH=`command -v patch >/dev/null && echo "1" || echo "0"`
  HAS_GIT=`command -v git >/dev/null && echo "1" || echo "0"`
  HAS_JAVA=`command -v java >/dev/null && echo "1" || echo "0"`

  if [ "$HAS_PATCH" = "0" -a "$HAS_GIT" = "0" ]; then
    echo "To install the software correctly we have to modify some files in contibuted modules."
    echo "This process requires the 'patch' or 'git' tools. Please install them first."
    exit;
  fi

  if [ "$HAS_JAVA" = "0" ]; then
    echo "To run Solr Java is a must have. Please install it."
    exit;
  fi
}

download_all() {
  if [ ! -e xc_installation-$DISTRO_VERSION-core.tar.gz ]; then
    download_requirement_check
    echo "Downloading distribution"
    download http://ftp.drupal.org/files/projects/xc_installation-$DISTRO_VERSION-core.tar.gz
  fi

  if [ ! -e jquery.ui-$JQUERY_UI_VERSION.zip ]; then
    download_requirement_check
    echo "Downloading jQuery UI"
    download http://jquery-ui.googlecode.com/files/jquery.ui-$JQUERY_UI_VERSION.zip
  fi

  if [ ! -e apache-solr-$SOLR_VERSION.tgz ]; then
    download_requirement_check
    echo "Downloading Apache Solr"
    download http://www.apache.org/dist/lucene/solr/$SOLR_VERSION/apache-solr-$SOLR_VERSION.tgz
  fi
}

rm -rf $DRUPAL_NAME
rm -rf apache-solr-$SOLR_VERSION-xc-1.1-test

requirement_check
download_all

CWD=`pwd`
echo "Prepare Drupal"
tar xzf xc_installation-$DISTRO_VERSION-core.tar.gz
mv drupal-6.22 $DRUPAL_NAME

cd $DRUPAL_NAME
if [ ! -e sites/default/settings.php ]; then
  cp sites/default/default.settings.php sites/default/settings.php
fi
chmod a+w sites/default/
chmod a+w sites/default/settings.php

# move modules and theme to sites/all
mv profiles/xc_installation/modules sites/all/modules
mv profiles/xc_installation/themes sites/all/themes

cd sites/all
if [ ! -d libraries ]; then
  mkdir libraries
fi

# patching fieldset_helper
echo "Patching fieldset_helper"
cd modules/fieldset_helper
pwd
cp ../xc/xc_util/patches/hook_fieldset_helper_path_alter-823318-4.patch .
cp fieldset_helper.module fieldset_helper.module.bak
patch -p1 fieldset_helper.module hook_fieldset_helper_path_alter-823318-4.patch

# go home directory
cd $CWD

# patching autocomplete
echo "Patching autocomplete"
cd $DRUPAL_NAME
pwd
cp sites/all/modules/xc/xc_util/patches/xc-autocomplete.patch .
cp misc/autocomplete.js misc/autocomplete.js.bak
patch -p1 misc/autocomplete.js xc-autocomplete.patch
rm xc-autocomplete.patch

# go home directory
cd $CWD

# jquery.ui
unzip -q jquery.ui-$JQUERY_UI_VERSION.zip
mv jquery.ui-$JQUERY_UI_VERSION jquery.ui
mv jquery.ui $DRUPAL_NAME/sites/all/libraries

echo "The preparation was successfull"

echo "The Solr part"
solr_instance=apache-solr-$SOLR_VERSION/example

echo '** extracting Solr...'
tar -zxf apache-solr-$SOLR_VERSION.tgz

conf_dir=$solr_instance/solr/conf
resources=$DRUPAL_NAME/sites/all/modules/xc/xc_solr/resources

# save original config files
cp $resources/solr.* $solr_instance
if [ ! -e $conf_dir/solrconfig-orig.xml ]; then
  mv $conf_dir/solrconfig.xml $conf_dir/solrconfig-orig.xml
  mv $conf_dir/schema.xml $conf_dir/schema-orig.xml
fi

# copy Solr configuration file
if [ -e $resources/solrconfig-for-$SOLR_VERSION.xml ]; then
  cp $resources/solrconfig-for-$SOLR_VERSION.xml $conf_dir/solrconfig.xml
else 
  cp $resources/solrconfig.xml $conf_dir
fi

# copy Solr schema file
if [ -e $resources/schema-for-$SOLR_VERSION.xml ]; then
  cp $resources/schema-for-$SOLR_VERSION.xml $conf_dir/schema.xml
else 
  cp $resources/schema.xml $conf_dir
fi

# copy jar files
SOLR_DIR=apache-solr-$SOLR_VERSION
SOLR_LIB=$solr_instance/solr/lib
mkdir $SOLR_LIB
cp $SOLR_DIR/dist/apache-solr-analysis-extras-$SOLR_VERSION.jar $SOLR_LIB
cp $SOLR_DIR/contrib/analysis-extras/lib/icu4j-4_8.jar $SOLR_LIB
cp $SOLR_DIR/contrib/analysis-extras/lucene-libs/lucene-icu-$SOLR_VERSION.jar $SOLR_LIB

cd $solr_instance
chmod +x solr.s*
cd $CWD

mv $SOLR_DIR $SOLR_DIR-xc-1.1-test

echo -n 'In which directory to put Apache Solr? ('$HOME'/solr): '
read SOLR_PARENT_DIR

if [ "$SOLR_PARENT_DIR" = "" ]; then
  SOLR_PARENT_DIR=$HOME/solr
fi

if [ ! -e $SOLR_PARENT_DIR ]; then
  mkdir $SOLR_PARENT_DIR
fi

if [ -e $SOLR_PARENT_DIR/$SOLR_DIR-xc-1.1-test ]; then
  rm -rf $SOLR_PARENT_DIR/$SOLR_DIR-xc-1.1-test
fi

mv $SOLR_DIR-xc-1.1-test $SOLR_PARENT_DIR

echo -n 'What is the Apache HTTP server web directory? (/var/www): '
read WWW_DIR

if [ "$WWW_DIR" = "" ]; then
  WWW_DIR=/var/www
fi

if [ -e $WWW_DIR/$DRUPAL_NAME ]; then
  rm -rf $WWW_DIR/$DRUPAL_NAME
fi

mv $DRUPAL_NAME $WWW_DIR

echo "Start Solr"
cd $SOLR_PARENT_DIR/$SOLR_DIR-xc-1.1-test/example
./solr.sh start
cd $CWD

