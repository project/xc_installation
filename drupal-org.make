core = 6.22
api = 2

; main module
projects[xc] = 1.1

; contrib modules
projects[addthis] = 3.0-beta1
projects[admin_menu] = 1.8
projects[fieldset_helper] = 1.0
projects[lightbox2] = 1.11
projects[print] = 1.12
projects[jquery_ui_dialog] = 1.10
projects[jquery_ui] = 1.5
projects[jquery_update] = 2.0-alpha1

; theme
projects[xc_theme] = 1.1
