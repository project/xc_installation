; Helpful links
; http://drupal.org/node/642116 

; Drupal core
api = 2
core = 6.x
projects[] = drupal

; XC profile
; NOTE: because of the recursive type of Drush make, it is not working right now
projects[xc_installation][type] = "profile"
projects[xc_installation][version] = "1.0-alpha3"

; core XC modules -- the latest
projects[xc][type] = "module"
projects[xc][download][type] = "git"
projects[xc][download][url] = "http://git.drupal.org/project/xc.git"
projects[xc][download][branch] = "demo"

; jquery.ui
libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
libraries[jquery_ui][directory_name] = "jquery.ui"
libraries[jquery_ui][destination] = "libraries"

